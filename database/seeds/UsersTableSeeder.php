<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('users')->delete();

        \DB::table('users')->insert(array (
            0 =>
            array (
                'id' => 1,
                'role_id' => 1,
                'name' => 'Luis Auz',
                'email' => 'luisauz.geek@gmail.com',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$sF10SHm0V0gopLY/saiP5uQrEtaSr0CCO2EfXzdULqYZiTwGqUdQy',
                'remember_token' => NULL,
                'settings' => '{"locale":"en"}',
                'created_at' => '2020-03-18 14:42:26',
                'updated_at' => '2020-04-16 20:00:36',
            ),
            1 =>
            array (
                'id' => 2,
                'role_id' => 2,
                'name' => 'Customer 1',
                'email' => 'customer1@b2bemprendiste.com',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$qWmYImXvJ6REfoSTfdTpFeej42txdt8e92F0.ZXN3uKqxxhO7Q2su',
                'remember_token' => NULL,
                'settings' => NULL,
                'created_at' => '2020-04-15 21:08:07',
                'updated_at' => '2020-04-15 21:08:07',
            ),
            2 =>
            array (
                'id' => 3,
                'role_id' => 2,
                'name' => 'Customer 2',
                'email' => 'customer2@b2bemprendiste.com',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$qWmYImXvJ6REfoSTfdTpFeej42txdt8e92F0.ZXN3uKqxxhO7Q2su',
                'remember_token' => NULL,
                'settings' => NULL,
                'created_at' => '2020-04-15 21:08:07',
                'updated_at' => '2020-04-15 21:08:07',
            ),
            3 =>
            array (
                'id' => 4,
                'role_id' => 2,
                'name' => 'Customer 3',
                'email' => 'customer3@b2bemprendiste.com',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$qWmYImXvJ6REfoSTfdTpFeej42txdt8e92F0.ZXN3uKqxxhO7Q2su',
                'remember_token' => NULL,
                'settings' => NULL,
                'created_at' => '2020-04-15 21:08:07',
                'updated_at' => '2020-04-15 21:08:07',
            ),
            4 =>
            array (
                'id' => 5,
                'role_id' => 3,
                'name' => 'Seller 1',
                'email' => 'seller1@b2bemprendiste.com',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$OiG1Rcw1j6Ua2DA.RICkp.v7Jy8z9axeLXpYMwm/xBUucC5L3jMyi',
                'remember_token' => NULL,
                'settings' => '{"locale":"en"}',
                'created_at' => '2020-04-16 19:59:43',
                'updated_at' => '2020-04-16 19:59:43',
            ),
            5 =>
            array (
                'id' => 6,
                'role_id' => 3,
                'name' => 'Seller 2',
                'email' => 'seller2@b2bemprendiste.com',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$EQ3GPaqE0AGh481LsUBAhuzQIVGo0dXyuqHsnsNvmhBv3xc8Okmai',
                'remember_token' => NULL,
                'settings' => '{"locale":"en"}',
                'created_at' => '2020-04-16 20:00:05',
                'updated_at' => '2020-04-16 20:00:05',
            ),
        ));


    }
}
